## Plays music from youtube in the terminal cause chrome is a bitch and takes up way too much memory

Requirements: 
* Youtube-dl- 

`sudo curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl` 

`sudo chmod a+rx /usr/local/bin/youtube-dl`

* sox- Figure out how to install
* redis- Figure out.
* python libraries mentioned at the top of the script

#### uses redis to store stuff and things are stored in songs. Stores 50 songs. 
Can be changed in the script by changing STORAGE_LIM

### Was not built for your use, was built for my use. If I get time, I will improve the script.

Things to further add - 
1. Ability to download youtube playlists
2. Ability to show youtube suggestions
3. Auto play from youtube
4. Some nice terminal gui
5. Writing a script to auto add dependencies, I'm looking at you npm. 
6. Trying real hard to make a build which other people can use too.

##### This is how it should behave
![Pic 4](https://image.ibb.co/f7arcF/Screen_Shot_2017_08_15_at_11_27_32_am.png "1.")
![Pic 3](https://image.ibb.co/bVgxHF/Screen_Shot_2017_08_15_at_11_28_32_am.png "2.")
![Pic 2](https://image.ibb.co/eABa4v/Screen_Shot_2017_08_15_at_11_29_11_am.png "3.")
![Pic 1](https://image.ibb.co/i2RmBa/Screen_Shot_2017_08_15_at_11_29_55_am.png "4.")

