import os
import sys
import subprocess
from lxml import html
import requests
import json
import redis
from lazyme.string import color_print as cp
#from bs4 import BeautifulSoup

#xp2 = '//*[@id="content"]/div/div/div/div[1]/div/div[2]/div[2]/a[*]/@href'
#f_pages = tree.xpath(xp2)
#print(f_pages)
YOUTUBE_SEARCH_URL = "https://www.youtube.com/results?search_query="
STORAGE_LIM = 50
sQuery = ''
videoReturnedTitles = ''
musicLibrary = []
redis_db = redis.client.Redis()

def startRedis():
	global redis_db
	subprocess.call("redis-server redis.conf &", shell=True)
	redis_db = redis.Redis(host='127.0.0.1', port=6379)
	

def getYoutubeSearchQuery():
	sQuery = raw_input('Go on mate, enter the song name: ')
	sQuery = sQuery.replace(' ','+')

	getVideoListFromYoutube(sQuery)


def getVideoListFromYoutube(sq):
	global videoReturnedTitles
	f_song_link = ""
	if(sq == 'h'):
		helpScreen()
		getYoutubeSearchQuery()


	if(sq != "r" and sq != "e" and sq != "off"):
		result = requests.get(YOUTUBE_SEARCH_URL+sq)
		#Print success based on the code here.
		print(result.status_code)
		c = result.content
		tree = html.fromstring(c)

		xp_var = '//*[@class="item-section"]/li[*]/div/div/div[2]/h3/a/'
		#print(tree.xpath("//*[@class='item-section']/li[1]/div/div/div[2]/h3/a/text()"))
		videoReturnedTitles = tree.xpath(xp_var+'text()')
		f_song_link = tree.xpath(xp_var+'@href')

		#print("Titles ")
		#print(videoReturnedTitles)
	elif(sq=="e"):
		sys.exit("Closing program...")

	elif(sq=="off"):
		print("Switching to offline")
		playOffline()


	selectedIndex = displaySongs(videoReturnedTitles)
	#print(f_song_link[selectedIndex][9:])
	#print f_song_link[selectedIndex]
	downloadSong(f_song_link[selectedIndex], videoReturnedTitles[selectedIndex])

def displaySongs(titles):
	c=0
	for x in titles:
		c=c+1
		print(str(c) + ". " + x)

	selection_choice = raw_input("Enter the number: ")
	if(selection_choice == "p"):
		getYoutubeSearchQuery()

	return(int(selection_choice)-1)

def downloadSong(songLink, songTitle):
	compressed_title = songLink[9:]
	redis_title_key = compressed_title+"_title"
	play_audio = 'play songs/' + compressed_title + ".mp3"
	res_red = redis_db.get(redis_title_key)
	if res_red is None: 
		print("Downloading from youtube and storing...")
		you_dl_cmd = 'youtube-dl --extract-audio --audio-format mp3 --output "songs/'+compressed_title+'.%(ext)s" https://www.youtube.com/watch?v='+compressed_title
		print(play_audio)
		redis_db.set(redis_title_key,songTitle)
		output = subprocess.check_output(you_dl_cmd, shell=True)
	else:
		print("Playing from local system...")
	
	addToHistory(compressed_title)
	playMusic(play_audio, 1) ;
	#subprocess.call(play_audio, shell=True)
	homeScreen()


def playMusic(audioFile, mode):
	subprocess.call(audioFile, shell=True)
	if(mode == 0):
		playOffline()
	else:
		getYoutubeSearchQuery()


def addToHistory(songTitle):
	#if redis_db.exists(songHistory) == 0:
		#redis_db.lpush(songHistory, "initv")
	redis_db.lrem('songHistory', songTitle, num=0)
	redis_db.lpush('songHistory', songTitle)
	if(redis_db.llen('songHistory')>STORAGE_LIM):
		fileToDelete = redis_db.rpop('songHistory') + ".mp3"
		cp("Trying to delete songs since the storage limit has been crossed...", cp='red')
		redis_db.delete(fileToDelete+"_title")
		os.remove(fileToDelete)


def playOffline():
	songKeys = redis_db.keys('*_title')
	songTitle = [None] * len(songKeys)

	c = 0 
	for song in songKeys:
		songTitle[c] = redis_db.get(song)
		c=c+1

	c = 1 	
	for title in songTitle:
		print(str(c) + ". "+ songTitle[c-1])
		c=c+1		

	cp("You can type on to go back to online", color='red')
	cp("You can type e to exit", color='red')

	songSelection = raw_input("Offline mode| Enter song number> ")
	if(songSelection == "on"):
		print("Switching to online")
		getYoutubeSearchQuery()

	elif(songSelection == "e"):
		sys.exit("Closing program...")

	elif(songSelection == "h"):
		helpScreen()
		playOffline()

	reverseKey = songKeys[int(songSelection)-1][::-1]
	reverseKey = reverseKey[6:]
	reverseKey = reverseKey[::-1]

	fullSongName = 'play songs/' + reverseKey + ".mp3"
	playMusic(fullSongName, 0)
	

def helpScreen():
	cp('Welcome to shitty help', color='blue', underline=True)
	cp('You can type e to exit', color='red')
	cp('You can type r to get your previous search result', color='red')
	cp('You can type off to swicth to offline mode in search screen', color='red')
	cp('you can type h for all commands at any input screen(does not work at all screens yet)', color='red')
	cp('you can type on to go to the online mode', color='red')
	cp('I guess p does something too, check the code, im lazy', color='red')
	cp('dont try to learn coding from it, its horrible coding practises', color='red')
	cp('And dont press anything else and experiment, it will just blow your system', color='red')
	cp('Has the script been tested? Yeah fuck you too', color='red')


def everythingElse():
	compressed_title = "".join(f_song_title[0].split())
	you_dl_cmd = 'youtube-dl --extract-audio --audio-format mp3 --output "'+compressed_title+'.%(ext)s" https://www.youtube.com/watch?v=_FrOQC-zEog'
	play_audio = 'play ' + compressed_title + ".mp3"
	print(play_audio)

	output = subprocess.check_output(you_dl_cmd, shell=True)
	print("Trying to play the song....")

	subprocess.call(play_audio, shell=True)


def testStuff():
	#print(YOUTUBE_SEARCH_URL+sq)
	result = requests.get("https://www.youtube.com/results?search_query=hey+you")
	#Print success based on the code here.
	print(result.status_code)
	c = result.content
	tree = html.fromstring(c)

	xp_var = '//*[@class="item-section"]/li[*]/div/div/div[2]/h3/a/'
	print(tree.xpath("//*[@class='item-section']/li[1]/div/div/div[2]/h3/a/text()"))
	tits = tree.xpath(xp_var+'text()')
	#f_song_link = tree.xpath(xp_var+'@href')

	print("Titles ")
	print(tits)


def homeScreen():
	getYoutubeSearchQuery()


startRedis()
cp('CODING COMPANION - YOUTUBE MUSIC EDITION, shitty alpha edition', color='blue', bold=True, underline=True)
cp('You can type e to exit', color='red')
cp('You can type r to get your previous search result', color='red')
cp('You can type off to swicth to offline mode', color='red')
cp('you can type h for all commands at any input screen(does not work at all screens yet)', color='red')


homeScreen()